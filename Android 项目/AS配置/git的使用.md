# git的使用
### 切换分支
1. `git checkout -b dev` 创建dev分支,并切换到dev分支
2. 相当于创建 `git branch dev` 和切换 `git checkout dev`
3. `git branch`查看当前分支,当前分支前面会标一个*号
4. 合并时需要切换到master, 然后 `git merge dev` 就是合并dev分支到当前分支(master), fast-forward ,快速合并分支
5. 删除dev分支 `git branch -d dev`


### 添加远程库
1. `$ git remote add origin 地址` 
2.  `git push -u origin master ` origin 为上面创建的远程仓库名字, -u推送和关联作用
3. `git push origin master` 第一次用上面推送, 以后就用这个
4. 克隆远程仓库 `git clone 地址`

### 忽略文件
1. 配置文件下载地址:  `https://github.com/github/gitignore`
2. 在文件根目录添加文件 `.gitignore`
3. 忽略文件添加后, 不能使用 `git add `添加文件
4. 强制添加 `git add -f `

### 配置别名
1. 比如使用 `git st` 代替status, 配置设置为`git config --global alias.st status`
2. 别的命令简写
```
$ git config --global alias.co checkout
$ git config --global alias.ci commit
$ git config --global alias.br branch
```
3. `git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"`

